MSB client implementation
==========================

Use this client lib to implement your own client for the MSB. See the client examples for how to use this client lib.


Use PIP to install this client:

    pip install msb-client

After installing the client take one of the examples and create you own client protocol. There are three ways to initiate an action:
 
 - implement an own version of the protocols _on_init()_ method to do something after the successfull connection to the MSB
 - create events which can either be trigged right on startup or with another reactor pattern like shown in _examples/barcode_scanner.py_
 - create functions and define an action to be taken in its callback
