#!/usr/bin/env python
# coding=utf-8
import logging


from msb_client.websocket_client import MSBClient

log = logging.getLogger('msb_client')


BROKER_URL = 'ws://192.168.188.94:8085/websocket/data/websocket'
# BROKER_URL = 'ws://ipa.virtualfortknox.de/msb/websocket/data/websocket'
CLIENT_CONFIG = {
    'class': 'SmartObject',
    'uuid': '6e76cb37-01aa-44d4-9d1d-f210a3fa4803',  # https://www.uuidgenerator.net/version4
    'token': 'licht',

    'name': 'Tuerlich(t)',
    'description': 'Katzenwarnlicht'
}


class LichtClient(MSBClient):

    def __init__(self, *args, **kwargs):

        super(LichtClient, self).__init__(*args, **kwargs)
        print('hab ich doch alles schon gemacht!')


if __name__ == '__main__':

    client = LichtClient(BROKER_URL, config=CLIENT_CONFIG)

    def handle_event(color):
        client.send_light(color)

    client.add_function({
        'functionId': "send_light",
        'name': "send light",
        'description': "Sends light to the display.",
        "dataFormat": {
            "message": {
                "$ref": "#/definitions/LightEvent"
            },
            "LightEvent": {
                "type": "object",
                "properties": {
                    "color": {"type": "string"}
                }
            }
        },
        'implementation': handle_event,
    })
    try:
        client.connect()
        client.run_forever()
    except KeyboardInterrupt:
        # close the websocket connection
        client.close()
