# coding=utf-8
from datetime import datetime
from random import choice
import logging

from msb_client.websocket_client import MSBClient

log = logging.getLogger('msb_client')


BROKER_URL = 'ws://52.59.33.3:8085/websocket/data/websocket'
# BROKER_URL = 'wss://52.59.33.3:8084/websocket/data/websocket'
CLIENT_CONFIG = {
    'class': 'SmartObject',
    'uuid': '55d88-34cf-4836-8cc1-7e0d9c5dca12c4',  # https://www.uuidgenerator.net/version4
    'token': 'py1',

    'name': 'Python sensor client',
    'description': 'Test client description goes here.'
}


class TestClient(MSBClient):

    def do_something(self):
        self.started = datetime.now()
        num_messages = 90
        log.info('doing something ..')
        for i in range(num_messages):
            self.send_event(
                'sensor_event',
                name=choice(['a', 'b', 'c']),
                timestamp=datetime.now().isoformat(),
                status=choice(['on', 'off']))

        self.finished = datetime.now()
        log.info('{} seconds for {} messages'.format(str(self.finished - self.started), str(num_messages)))
        return

if __name__ == '__main__':

    client = TestClient(BROKER_URL, config=CLIENT_CONFIG)
    client.add_event({
        'name': "Sensor event",
        'eventId': 'sensor_event',
        'description': "Description for Event 1",
        'dataFormat': {
            'dataObject': {"$ref": "#/definitions/SensorEvent"},
            'SensorEvent': {
                'type': "object",
                'properties': {
                    'timestamp': {'type': "string"},
                    'name': {'type': "string"},
                    'status': {'type': "string"}
                }
            }
        },
        'implementation': {
            'priority': 2,
            'dataObject': {
                'timestamp': '2016-07-24 12:00:45',
                'name': "sensor1",
                'status': 'on'
            }
        }
    })
    try:
        client.connect()
        client.run_forever()
    except KeyboardInterrupt:
        client.close()





