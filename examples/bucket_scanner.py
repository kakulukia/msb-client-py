#!/usr/bin/env python
# coding=utf-8
import logging

from twisted.internet import reactor
from datetime import datetime
from msb_client.twisted_ws_client import MSBClientFactory, MSBClientProtocol

log = logging.getLogger("msb_client")
log.setLevel(logging.INFO)


BROKER_URL = 'ws://192.168.188.71:8085/websocket/data/websocket'
CLIENT_CONFIG = {
    'class': u'Application',
    'uuid': u'7d88-34cf-4836-8cc1-7e0d345dca124',  # https://www.uuidgenerator.net/version4
    'token': u'service',

    'name': u'Präsentationssservice',
    'description': u'Dieser Service dient dazu die Funktionsweise des MSB zu veranschaulichen.'
}


class ScannerProtocol(MSBClientProtocol):
    def do_something(self):
        print('initializing Scanner ..')

        import RPi.GPIO as GPIO

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(15, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(15, GPIO.BOTH)
        def my_callback(*args, **kwargs):
            print('triggered ..')
            self.send_event('item_dropped', timestamp=str(datetime.now()))
        GPIO.add_event_callback(15, my_callback)


factory = MSBClientFactory(BROKER_URL, config=CLIENT_CONFIG, debug=True, protocol=ScannerProtocol)


factory.add_event({
    'name': "Item dropped",
    'eventId': 'item_dropped',
    'description': "A new item has been dropped in the basket.",
    'dataFormat': {
        'dataObject': {"$ref": "#/definitions/ItemDropped"},
        'ItemDropped': {
            'type': "object",
            'properties': {
                'timestamp': {'type': "string"},
            }
        }
    },
    'implementation': {
        'priority': 2,
        'dataObject': {
            'color': str(datetime.now()),
        }
    }
})

reactor.connectTCP(factory.get_domain(), 8085, factory)
reactor.run()
