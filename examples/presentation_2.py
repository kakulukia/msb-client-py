#!/usr/bin/env python
# coding=utf-8
import logging

from twisted.internet import reactor

from msb_client.twisted_ws_client import MSBClientFactory, MSBClientProtocol

log = logging.getLogger("msb_client")
log.setLevel(logging.INFO)


BROKER_URL = 'ws://192.168.188.92:8085/websocket/data/websocket'
CLIENT_CONFIG = {
    'class': u'Application',
    'uuid': u'7d88-34cf-4836-8cc1-7e0d345dca124',  # https://www.uuidgenerator.net/version4
    'token': u'service',

    'name': u'Präsentationssservice',
    'description': u'Dieser Service dient dazu die Funktionsweise des MSB zu veranschaulichen.'
}


class ScannerProtocol(MSBClientProtocol):

    def do_something(self):
        print('never did anything .. :/')
        self.send_event('light_event', color='#ff00ff')


factory = MSBClientFactory(BROKER_URL, config=CLIENT_CONFIG, debug=True, protocol=ScannerProtocol)


def trigger_sensor(*args, **kwargs):
    print('triggered..', args, kwargs)

factory.add_function({
    'functionId': "analyse_sensor",
    'name': "analyse sensor",
    'description': "Will print the string inside the received message.",
    "dataFormat": {
        "message": {
            "$ref": "#/definitions/SensorEvent"
        },
        "SensorEvent": {
            "type": "object",
            "properties": {
                "timestamp": {"type": "string"},
                "name": {"type": "string"},
                "status": {"type": "string"},
            }
        }
    },
    'implementation': trigger_sensor,
})

factory.add_event({
    'name': "Send Light",
    'eventId': 'light_event',
    'description': "Sends light to the door display.",
    'dataFormat': {
        'dataObject': {"$ref": "#/definitions/LightEvent"},
        'LightEvent': {
            'type': "object",
            'properties': {
                'color': {'type': "string"},
            }
        }
    },
    'implementation': {
        'priority': 2,
        'dataObject': {
            'color': 'ff00ff',
        }
    }
})

reactor.connectTCP(factory.get_domain(), 8085, factory)
reactor.run()
