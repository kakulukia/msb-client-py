#!/usr/bin/env python
# coding=utf-8
import logging

from twisted.internet import reactor
from datetime import datetime, timedelta
from msb_client.twisted_ws_client import MSBClientFactory, MSBClientProtocol

log = logging.getLogger("msb_client")
log.setLevel(logging.INFO)


BROKER_URL = 'ws://192.168.188.94:8085/websocket/data/websocket'
CLIENT_CONFIG = {
    'class': u'SmartObject',
    'uuid': u'7d88-34cf-4112-8cc1-7e0d345dca124',  # https://www.uuidgenerator.net/version4
    'token': u'button',

    'name': u'SyncButton',
    'description': u'Dieser Client sendet ein Signal, jedes Mal, wenn der SynchronisierungsButton gedrueckt wird.'
}


class ButtonProtocol(MSBClientProtocol):

    cool_down = None

    def on_init(self):
        print('initializing Button ..')

        from RPi import GPIO

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        def my_callback(*args, **kwargs):

            if not self.cool_down or (datetime.now() - self.cool_down) > timedelta(minutes=1):
                print('sending')
                self.cool_down = datetime.now()
                self.send_event('button_pressed', timestamp=str(datetime.now()))
            else:
                print('no so fast, lil padawan!')

        GPIO.add_event_detect(10, GPIO.RISING, bouncetime=500, callback=my_callback)


factory = MSBClientFactory(BROKER_URL, config=CLIENT_CONFIG, debug=True, protocol=ButtonProtocol)


factory.add_event({
    'name': "Button pressed",
    'eventId': 'button_pressed',
    'description': "The sync button has been pressed.",
    'dataFormat': {
        'dataObject': {"$ref": "#/definitions/ButtonPressed"},
        'ButtonPressed': {
            'type': "object",
            'properties': {
                'timestamp': {'type': "string"},
            }
        }
    },
    'implementation': {
        'priority': 2,
        'dataObject': {
            'timestamp': str(datetime.now()),
        }
    }
})

reactor.connectTCP(factory.get_domain(), 8085, factory)
reactor.run()
