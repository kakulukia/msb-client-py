#!/usr/bin/env python
# coding=utf-8
import logging
from time import sleep

from twisted.internet import reactor, stdio
from twisted.protocols.basic import LineReceiver

from msb_client.twisted_ws_client import MSBClientFactory, MSBClientProtocol

log = logging.getLogger("msb_client")
log.setLevel(logging.INFO)


BROKER_URL = 'ws://ws.msb.bot.virtualfortknox.de/websocket/data/websocket'
CLIENT_CONFIG = {
    'class': u'SmartObject',
    'uuid': u'7d88-34cf-4836-834cc1-7sdfge0d3376ca124',  # https://www.uuidgenerator.net/version4
    'token': u'scanner2',

    'name': u'Barcode Scanner',
    'description': u'Dieser Service dient dazu die Funktionsweise des MSB zu veranschaulichen.'
}

msb_client = None

# initialize the LED
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)

def set_color(color):
    GPIO.output(11, GPIO.HIGH)
    GPIO.output(13, GPIO.HIGH)
    GPIO.output(15, GPIO.HIGH)

    if color == 'red':
        GPIO.output(11, GPIO.LOW)
    elif color == 'green':
        GPIO.output(13, GPIO.LOW)
    elif color == 'blue':
        GPIO.output(15, GPIO.LOW)

set_color('red')


class ScannerProtocol(MSBClientProtocol):

    def on_init(self):
        global msb_client
        msb_client = self
        print('enter your codes ..')
        set_color('green')

class ScannerFactory(MSBClientFactory):
    def clientConnectionLost(self, connector, reason):
        set_color('red')
        super().clientConnectionLost(connector, reason)

    def clientConnectionFailed(self, connector, reason):
        set_color('red')
        super().clientConnectionFailed(connector, reason)

factory = ScannerFactory(BROKER_URL, config=CLIENT_CONFIG, debug=True, protocol=ScannerProtocol)


factory.add_event({
    'name': "Send Code",
    'eventId': 'send_code',
    'description': "Sends a scanned barcode to the MSB.",
    'dataFormat': {
        'dataObject': {"$ref": "#/definitions/BarCode"},
        'BarCode': {
            'type': "object",
            'properties': {
                'barcode': {'type': "string"},
            }
        }
    },
    'implementation': {
        'priority': 2,
        'dataObject': {
            'barcode': '',
        }
    }
})

class Echo(LineReceiver):
    delimiter = b'\n'

    def lineReceived(self, line):
        if msb_client and msb_client.transport.connected:
            set_color('blue')
            msb_client.send_event('send_code', barcode=line)
            sleep(0.2)
            set_color('green')

reactor.connectTCP(factory.get_domain(), 8085, factory, timeout=10)
stdio.StandardIO(Echo())
reactor.run()
