#!/usr/bin/env python
# coding=utf-8
import logging
import signal
from datetime import timedelta

from dateutil.parser import parse

from msb_client.websocket_client import MSBClient

log = logging.getLogger("msb_client")
log.setLevel(logging.INFO)

BROKER_URL = 'ws://192.168.188.71:8085/websocket/data/websocket'
# BROKER_URL = 'ws://ipa.virtualfortknox.de/msb/websocket/data/websocket'
CLIENT_CONFIG = {
    'class': 'Application',
    'uuid': '7d88-34cf-4836-8cc1-7e0d345dca12c4',  # https://www.uuidgenerator.net/version4
    'token': 'service',

    'name': 'Präsentationssservice',
    'description': 'Dieser Service dient dazu die Funktionsweise des MSB zu veranschaulichen.'
}


class Sensor:
    on = None
    off = None


"""
         |a c|
left  <- |   | ->  right
"""
sensors = {
    'a': Sensor(),
    'b': Sensor(),
    'c': Sensor(),
}
direction = None

timer = None
wrong_goods = 0


def run_service():

    client = MSBClient(BROKER_URL, config=CLIENT_CONFIG, debug=True)
    # ensure the websocket connection is closed correctly after program termination.
    signal.signal(signal.SIGTERM, client.close)

    def in_or_out(going_left, timestamp):
        """
        This service is determining the direction of made-up goods on a conveying belt.
        The goods shall go from right to left. Going in the other direction shall result in a yellow light.
        The next sensor activation in the right direction shall reset the light to black.
        In case multiple goods are going the wrong direction (2), it shall count the goods and display a red light.

        :param going_left:
        :param timestamp:
        """

        global direction, wrong_goods

        if not going_left:
            wrong_goods = min([wrong_goods + 1, 2])
            if wrong_goods >= 2:
                # red light
                client.send_event('light_event', color='ff0000')
            else:
                # yellow light
                client.send_event('light_event', color='ffd700')
        else:
            if wrong_goods:
                wrong_goods -= 1

                if wrong_goods == 1:
                    client.send_event('light_event', color='ffd700')
                elif not wrong_goods:
                    client.send_event('light_event', color='00ff00')
            else:
                client.send_event('light_event', color='00ff00')

    def trigger_sensor(name, status, timestamp):

        timestamp = parse(timestamp)
        global timer

        if name not in sensors.keys():
            return

        if status == 'on':
            sensors[name].on = timestamp
        else:
            sensors[name].off = timestamp

            if name == 'a':
                if sensors['c'].off and (timestamp - sensors['c'].off) < timedelta(seconds=1):
                    in_or_out(True, timestamp)

            if name == 'c':
                if sensors['a'].off and (timestamp - sensors['a'].off) < timedelta(seconds=1):
                    in_or_out(False, timestamp)

    client.add_function({
        'functionId': "analyse_sensor",
        'name': "analyse sensor",
        'description': "Will print the string inside the received message.",
        "dataFormat": {
            "message": {
                "$ref": "#/definitions/SensorEvent"
            },
            "SensorEvent": {
                "type": "object",
                "properties": {
                    "timestamp": {"type": "string"},
                    "name": {"type": "string"},
                    "status": {"type": "string"},
                }
            }
        },
        'implementation': trigger_sensor,
    })

    client.add_event({
        'name': "Send Light",
        'eventId': 'light_event',
        'description': "Sends light to the door display.",
        'dataFormat': {
            'dataObject': {"$ref": "#/definitions/LightEvent"},
            'LightEvent': {
                'type': "object",
                'properties': {
                    'color': {'type': "string"},
                }
            }
        },
        'implementation': {
            'priority': 2,
            'dataObject': {
                'color': 'ff00ff',
            }
        }
    })

    try:
        client.connect()
        client.run_forever()
    except KeyboardInterrupt:
        client.close()


run_service()
