#!/usr/bin/env python
# coding=utf-8
import logging

from autobahn.twisted.websocket import connectWS
from twisted.internet import reactor
from msb_client.twisted_ws_client import MSBClientFactory, MSBClientProtocol

log = logging.getLogger("msb_client")
log.setLevel(logging.DEBUG)


BROKER_URL = 'ws://ws.msb.bot.virtualfortknox.de/websocket/data/websocket'
CLIENT_CONFIG = {
    'class': u'Application',
    'uuid': u'7d88-34cf-4122-8cc1-7e0d345dca124',  # https://www.uuidgenerator.net/version4
    'token': u'sync',

    'name': u'SyncSerice',
    'description': u'Dieser Service startet den eigentlichen Synchronisierungsvorgang sobald ein Signal eingeht.'
}


factory = MSBClientFactory(BROKER_URL, config=CLIENT_CONFIG, debug=True, protocol=MSBClientProtocol)


def trigger_sync():
    print("syncing ..")


factory.add_function({
    'name': "Start Synchronization",
    'functionId': 'start_sync',
    'description': "The sync button has been pressed, so start the sync process.",
    'dataFormat': {
        'dataObject': {"$ref": "#/definitions/ButtonPressed"},
        'ButtonPressed': {
            'type': "object",
            'properties': {
                'timestamp': {'type': "string"},
            }
        }
    },
    'implementation': trigger_sync
})

connectWS(factory)
reactor.run()
