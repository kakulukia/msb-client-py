#!/usr/bin/env python
# coding=utf-8
import logging
from datetime import timedelta
from threading import Timer

from dateutil.parser import parse

from msb_client.websocket_client import MSBClient

log = logging.getLogger("msb_client")
log.setLevel(logging.INFO)

BROKER_URL = 'ws://192.168.188.92:8085/websocket/data/websocket'
# BROKER_URL = 'ws://ipa.virtualfortknox.de/msb/websocket/data/websocket'
CLIENT_CONFIG = {
    'class': 'Application',
    'uuid': '7d88-34cf-4836-8cc1-7e0d9c5dca12c4',  # https://www.uuidgenerator.net/version4
    'token': 'cat',

    'name': 'CATegorizer service',
    'description': 'Tries to distinguish cats from people passing the office door.'
}


class Sensor:
    on = None
    off = None


"""
          |   |
          |   |
          | b |
          |   |
          |a c|
inside <- |   | -> outside
"""
sensors = {
    'a': Sensor(),
    'b': Sensor(),
    'c': Sensor(),
}


timer = None
cats = 0
humans = 0


def run_categorizer():

    client = MSBClient(BROKER_URL, config=CLIENT_CONFIG, debug=True)

    def cat_or_no_cat(going_inside, timestamp):

        global humans
        global cats

        if sensors['b'].off and (timestamp - sensors['b'].off) < timedelta(seconds=1):
            if going_inside:
                humans += 1
            else:
                humans = humans - 1 if humans > 0 else 0
        else:
            if going_inside:
                cats += 1
            else:
                cats = cats - 1 if cats > 0 else 0

        print('humans: {} // cats: {}'.format(humans, cats))

        if cats > 0:
            client.send_event('light_event', color='ff0000')
        else:
            if humans:
                client.send_event('light_event', color='00ff00')
            else:
                client.send_event('light_event', color='000000')

    def trigger_sensor(name, status, timestamp):

        timestamp = parse(timestamp)
        global timer

        if name not in sensors.keys():
            return

        if status == 'on':
            sensors[name].on = timestamp
        else:
            sensors[name].off = timestamp

            if name == 'a':
                if sensors['c'].off and (timestamp - sensors['c'].off) < timedelta(seconds=1):

                    # we have someone going inside
                    # the Timer is used to take the second leg into account and fire once someone
                    # actually fully went through
                    if timer:
                        timer.cancel()
                    timer = Timer(1, cat_or_no_cat, (True, timestamp))
                    timer.start()

            if name == 'c':
                if sensors['a'].off and (timestamp - sensors['a'].off) < timedelta(seconds=1):
                    # we have someone going inside
                    if timer:
                        timer.cancel()
                    timer = Timer(1, cat_or_no_cat, (False, timestamp))
                    timer.start()

    # local callback is needed otherwise we will have a pickle error
    # so in case the callback is outside this very function,you might need it :)
    def callback(**kwargs):
        trigger_sensor(**kwargs)

    client.add_function({
        'functionId': "analyse_sensor",
        'name': "analyse sensor",
        'description': "Will print the string inside the received message.",
        "dataFormat": {
            "message": {
                "$ref": "#/definitions/SensorEvent"
            },
            "SensorEvent": {
                "type": "object",
                "properties": {
                    "timestamp": {"type": "string"},
                    "name": {"type": "string"},
                    "status": {"type": "string"},
                }
            }
        },
        'implementation': callback,
    })

    client.add_event({
        'name': "Send Light",
        'eventId': 'light_event',
        'description': "Sends light to the door display.",
        'dataFormat': {
            'dataObject': {"$ref": "#/definitions/LightEvent"},
            'LightEvent': {
                'type': "object",
                'properties': {
                    'color': {'type': "string"},
                }
            }
        },
        'implementation': {
            'priority': 2,
            'dataObject': {
                'color': 'ff00ff',
            }
        }
    })

    try:
        client.connect()
        client.run_forever()
    except KeyboardInterrupt:
        client.close()


run_categorizer()
